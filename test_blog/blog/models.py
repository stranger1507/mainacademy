from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _


class Post(models.Model):
    text = models.TextField(_('Текст поста'))
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name=_('Автор'), on_delete=models.CASCADE
    )
    title = models.CharField(_('Заголовок'), max_length=255)
    created_datetime = models.DateTimeField(_('Дата и время создания'), auto_now_add=True)
    edited_datetime = models.DateTimeField(_('Дата и время редактирования'), auto_now=True)
    ordering = models.PositiveIntegerField(_('Номер поста'), null=True, blank=True)
    is_published = models.BooleanField(_('Пост опубликован?'), default=False)

    def __str__(self):
        return '{0} - {1}'.format(self.user.email, self.title)

    class Meta:
        verbose_name = _('Объявление')
        verbose_name_plural = _('Объявления')


class Comment(models.Model):
    text = models.TextField(_('Текст поста'))
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_('Автор'),
                             on_delete=models.CASCADE)
    post = models.ForeignKey('Post', on_delete=models.CASCADE, verbose_name=_('Объявление'))
    created_datetime = models.DateTimeField(_('Дата и время создания'), auto_now_add=True)
    edited_datetime = models.DateTimeField(_('Дата и время редактирования'), auto_now=True)

    def __str__(self):
        return '{0} - {1}'.format(self.user.email, self.post.id)

    class Meta:
        verbose_name = _('Комментарий')
        verbose_name_plural = _('Комментарии')

from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect, JsonResponse
from django.template.loader import render_to_string


class AddUserToFormMixin(LoginRequiredMixin):
    def form_invalid(self, form):
        super(AddUserToFormMixin, self).form_invalid(form)
        return JsonResponse({
            'status':'error',
            'html': render_to_string(self.ajax_template_name, {'form': form}, request=self.request)
        })

    def form_valid(self, form):
        new_form = form.save(commit=False)
        new_form.user = self.request.user
        new_form.save()
        return JsonResponse({'status': 'ok'})


class DeleteFormMixin(LoginRequiredMixin):

    def delete(self, request, *args, **kwargs):
        super(DeleteFormMixin, self).delete(request, *args, **kwargs)
        return JsonResponse({'status': 'ok'})

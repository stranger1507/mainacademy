from pprint import pprint
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from bs4 import BeautifulSoup
import requests

from blog.models import Post


class Command(BaseCommand):
    help = 'Парсим lorem ipsum'

    def handle(self, *args, **options):
        data = {
            'amount': 100,
            'what': 'paras',
            'start': 'yes'
        }
        url = 'https://ru.lipsum.com/feed/html'
        response = requests.post(url, data)
        user = get_user_model().objects.first()
        if response.status_code == 200:
            soup = BeautifulSoup(response.text, 'html.parser')
            for p in soup.find('div', {'class': 'boxed'}).find_all('p'):
                text = p.text.strip()
                Post.objects.create(user=user, text=text, title=text[:10], is_published=True)

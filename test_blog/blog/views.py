from django.shortcuts import get_object_or_404
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect, JsonResponse
from django.template.loader import render_to_string
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from rest_framework import viewsets

from .forms import CreatePostForm, CreateCommentForm
from .mixins import AddUserToFormMixin, DeleteFormMixin
from .models import Post, Comment
from .serializers import PostSerializer


class IndexListView(ListView):
    queryset = Post.objects.all().order_by('-id')
    template_name = 'index.html'
    context_object_name = 'posts'
    paginate_by = 3


class PostDetailView(DetailView):
    model = Post
    template_name = 'detail.html'
    context_object_name = 'post'

    def get_context_data(self, **kwargs):
        context = super(PostDetailView, self).get_context_data(**kwargs)
        context['comment_form'] = CreateCommentForm
        context['comment_url'] = reverse_lazy('comment_create', kwargs={'pk': self.kwargs['pk']})
        return context


class PostCreateView(AddUserToFormMixin, CreateView):
    model = Post
    template_name = 'create_form.html'
    success_url = '/'
    form_class = CreatePostForm
    ajax_template_name = 'create_form_ajax.html'


class PostUpdateView(AddUserToFormMixin, UpdateView):
    model = Post
    template_name = 'create_form.html'
    success_url = '/'
    form_class = CreatePostForm
    ajax_template_name = 'create_form_ajax.html'


class PostDeleteView(DeleteFormMixin, DeleteView):
    model = Post
    template_name = 'delete_form.html'
    ajax_template_name = ''
    success_url = '/'
    fields = '__all__'
    #
    # def get(self, *args, **kwargs):
    #     # post = get_object_or_404(Post, id=self.kwargs['pk'])
    #     post = Post.objects.get(id=self.kwargs['pk'])
    #     if post.user_id == self.request.user.id:
    #         return super(PostDeleteView, self).get(*args, **kwargs)
    #     else:
    #         return HttpResponseRedirect(
    #             reverse_lazy('post_detail', kwargs={'pk': self.kwargs['pk']}))
    #
    # def post(self, *args, **kwargs):
    #     # post = get_object_or_404(Post, id=self.kwargs['pk'])
    #     post = Post.objects.get(id=self.kwargs['pk'])
    #     if post.user_id == self.request.user.id:
    #         return super(PostDeleteView, self).post(*args, **kwargs)
    #     else:
    #         return HttpResponseRedirect(
    #             reverse_lazy('post_detail', kwargs={'pk': self.kwargs['pk']}))


class CommentCreateView(AddUserToFormMixin, CreateView):
    model = Comment
    form_class = CreateCommentForm
    template_name = 'create_form.html'
    success_url = '/'

    def get_success_url(self):
        return reverse_lazy('post_detail', kwargs={'pk': self.kwargs['pk']})

    def get_context_data(self, **kwargs):
        context = super(CommentCreateView, self).get_context_data(**kwargs)
        context['comment_url'] = reverse_lazy('comment_create', kwargs={'pk': self.kwargs['pk']})
        return context

    def form_valid(self, form):
        new_form = form.save(commit=False)
        new_form.user = self.request.user
        new_form.post_id = self.kwargs['pk']
        new_form.save()
        return HttpResponseRedirect(self.get_success_url())


class CommentUpdateView(AddUserToFormMixin, UpdateView):
    model = Comment
    form_class = CreateCommentForm
    template_name = 'create_form.html'
    success_url = '/'

    def get_context_data(self, **kwargs):
        context = super(CommentUpdateView, self).get_context_data(**kwargs)
        context['comment_url'] = reverse_lazy(
            'comment_update', kwargs={'pk': self.object.id, 'post_id': self.object.post.id}
        )
        return context

    def get_success_url(self):
        return reverse_lazy('post_detail', kwargs={'pk': self.kwargs['post_id']})

    def form_valid(self, form):
        new_form = form.save(commit=False)
        new_form.user = self.request.user
        new_form.post_id = self.kwargs['post_id']
        new_form.save()
        return HttpResponseRedirect(self.get_success_url())


class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.none()
    serializer_class = PostSerializer

    def get_queryset(self):
        if self.request.user.is_authenticated:
            return Post.objects.filter(user=self.request.user)
        return self.queryset

from captcha.fields import CaptchaField, CaptchaTextInput
from django import forms
from django.utils.translation import gettext_lazy as _

from .models import Post, Comment


class CreatePostForm(forms.ModelForm):
    captcha = CaptchaField(
        widget=CaptchaTextInput(attrs={'placeholder': _('Введите капчу')}), label=_('Капча'),
    )
    text = forms.CharField(widget=forms.Textarea(
        attrs={
            'placeholder': 'Введи текст',
            'class': 'form-control'
        }),
    )
    title = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': 'form-control'
        }),
    )

    class Meta:
        model = Post
        fields = ('text', 'title',)


class CreateCommentForm(forms.ModelForm):
    captcha = CaptchaField(
        widget=CaptchaTextInput(attrs={'placeholder': _('Введите капчу')}), label=_('Капча'),
    )
    text = forms.CharField(widget=forms.Textarea(
        attrs={
            'placeholder': 'Введи текст',
            'class': 'form-control'
        }),
        label=_('Ваш комментарий')
    )

    class Meta:
        model = Comment
        fields = ('text',)

from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from django.conf.urls.static import static
from django.conf import settings
from accounts.views import UserViewSet
from . import views

router = DefaultRouter()
router.register(r'user', UserViewSet)
router.register(r'posts', views.PostViewSet)

urlpatterns = [
    url(r'^$', views.IndexListView.as_view(), name='posts_list'),
    url(r'^detail/(?P<pk>\d+)/$', views.PostDetailView.as_view(), name='post_detail'),
    url(r'^create/comment/(?P<pk>\d+)/$', views.CommentCreateView.as_view(), name='comment_create'),
    url(r'^update/comment/(?P<pk>\d+)/(?P<post_id>\d+)$', views.CommentUpdateView.as_view(), name='comment_update'),
    url(r'^create/$', views.PostCreateView.as_view(), name='post_create'),
    url(r'^update/(?P<pk>\d+)/$', views.PostUpdateView.as_view(), name='post_update'),
    url(r'^delete/(?P<pk>\d+)/$', views.PostDeleteView.as_view(), name='post_delete'),
]

urlpatterns += router.urls


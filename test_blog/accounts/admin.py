from django.contrib import admin
from django.contrib.auth import get_user_model

from .forms import CustomUserCreationForm


@admin.register(get_user_model())
class UserAdmin(admin.ModelAdmin):
    list_display = ['email', 'date_joined', 'is_staff', 'avatar']
    add_form = CustomUserCreationForm

import json
from django.conf import settings
from django.contrib.auth import get_user_model
from pprint import pprint
from django.test import TestCase
from django.urls import reverse
import requests
from rest_framework import status
from rest_framework.renderers import JSONRenderer

from .factories import UserFactory
from ..models import User
from ..views import UserViewSet



class TestUserEndpoints(TestCase):
    """
    Test endpoints for user (agent or contributor) role.
    """

    def setUp(self):
        self.user = UserFactory()
        self.client.login(email=self.user.email, password='1234')

    def test_list(self):
        for x in range(20):
            UserFactory()
        response = self.client.get(reverse('user-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data['results'][0].get('email'),
            get_user_model().objects.all().order_by('-id').first().email
        )
        self.assertEqual(len(response.data['results']), settings.REST_FRAMEWORK['PAGE_SIZE'])

    def test_detail(self):
        response = self.client.get(reverse('user-detail', kwargs={'pk': self.user.id}), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['email'], self.user.email)
        self.assertEqual(response.data['id'], self.user.id)

    def test_delete(self):
        response = self.client.get(reverse('user-detail', kwargs={'pk': self.user.id}), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client.delete(reverse('user-detail', kwargs={'pk': self.user.id}), format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        response = self.client.get(reverse('user-detail', kwargs={'pk': self.user.id}),format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_patch(self):
        old_data = self.user.first_name
        new_data = {'first_name': 'qwe'}
        url = reverse('user-detail', kwargs={'pk': self.user.id})
        response = self.client.patch(url, json.dumps(new_data), content_type='application/json', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client.get(reverse('user-detail', kwargs={'pk': self.user.id}),format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotEqual(response.data['first_name'], old_data)
        self.assertEqual(response.data['first_name'], new_data['first_name'])

    def test_put(self):
        new_data = {
          "first_name": "string",
          "last_name": "string",
          "email": self.user.email
        }
        url = reverse('user-detail', kwargs={'pk': self.user.id})
        response = self.client.get(reverse('user-detail', kwargs={'pk': self.user.id}),
                                   format='json')
        self.assertNotEqual(response.data['first_name'], new_data['first_name'])
        self.assertNotEqual(response.data['last_name'], new_data['last_name'])
        response = self.client.patch(url, json.dumps(new_data), content_type='application/json', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client.get(reverse('user-detail', kwargs={'pk': self.user.id}),format='json')
        self.assertEqual(response.data['first_name'], new_data['first_name'])
        self.assertEqual(response.data['last_name'], new_data['last_name'])


import factory  # pylint: disable=import-error
from django.contrib.auth import get_user_model


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = get_user_model()

    first_name = factory.Sequence(lambda n: 'Johnny{0}'.format(n))
    last_name = 'Doe'
    email = factory.Sequence(lambda n: 'johnny{0}@unknown.man'.format(n))
    password = factory.PostGenerationMethodCall('set_password', '1234')

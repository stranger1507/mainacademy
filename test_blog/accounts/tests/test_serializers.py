from django.test import TestCase

from .factories import UserFactory
from ..serializers import UserSerializer


class TestUserSerializer(TestCase):

    def setUp(self):
        self.user = UserFactory()

    def test_get_valid_user_fields(self):
        serializer = UserSerializer(instance=self.user)
        self.assertEqual(serializer.data.get('email'), self.user.email)
        self.assertEqual(serializer.data.get('id'), self.user.id)
        self.assertIsNone(serializer.data.get('password'))
        self.assertEqual(serializer.data.get('who_is'), str(self.user.get_short_name))

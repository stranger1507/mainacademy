from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.utils.translation import ugettext_lazy as _

from .managers import UserManager


class User(AbstractBaseUser, PermissionsMixin):
    """
    Custom User model.
    """

    first_name = models.CharField(_('First name'), max_length=50)
    last_name = models.CharField(_('Last name'), max_length=50)
    email = models.EmailField(_('Email'), null=True, blank=True, unique=True)
    date_joined = models.DateTimeField(_('Date joined'), auto_now_add=True)
    is_staff = models.BooleanField(_('Is staff'), default=False)
    avatar = models.ImageField(_('Avatar'), null=True, blank=True, upload_to='avatars')
    objects = UserManager()

    USERNAME_FIELD = 'email'
    EMAIL_FIELD = 'email'
    REQUIRED_FIELDS = []

    @property
    def get_short_name(self):
        return self.first_name

    @property
    def get_full_name(self):
        """
        Returns user's first and last name concatenation
        """
        if self.first_name and self.last_name:
            return '{0} {1}'.format(self.first_name, self.last_name)
        return 'anonymous'

    def __str__(self):
        return self.get_full_name

    class Meta:
        verbose_name_plural = _('Пользователи')
        verbose_name = _('Пользователь')
        ordering = ('-date_joined',)

from braces.views import AnonymousRequiredMixin
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView
from django.urls import reverse_lazy
from django.views.generic import DetailView, CreateView, UpdateView
from rest_framework.viewsets import ModelViewSet

from .serializers import UserSerializer
from .forms import LoginForm, CustomUserCreationForm, CustomUserUpdateForm


class UserDetailView(DetailView):
    template_name = 'user_detail.html'
    model = get_user_model()


class CustomLoginView(AnonymousRequiredMixin, LoginView):
    template_name = 'accounts/login.html'
    succes_url = '/'
    form_class = LoginForm


class CustomLogoutView(LogoutView):
    next_page = settings.LOGOUT_REDIRECT_URL


class RegisterCreateView(AnonymousRequiredMixin, CreateView):
    model = get_user_model()
    form_class = CustomUserCreationForm
    success_url = None
    template_name = 'registration.html'

    def get_success_url(self):
        return reverse_lazy('login')

class UserUpdateView(LoginRequiredMixin, UpdateView):
    model = get_user_model()
    form_class = CustomUserUpdateForm
    success_url = None
    template_name = 'registration.html'

    def get_success_url(self):
        return reverse_lazy('login')


class UserViewSet(ModelViewSet):
    serializer_class = UserSerializer
    queryset = get_user_model().objects.all().order_by('-id')

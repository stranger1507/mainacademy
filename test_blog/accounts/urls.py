from accounts.views import UserDetailView
from django.conf.urls import url

from .views import CustomLoginView, CustomLogoutView, RegisterCreateView, UserUpdateView

urlpatterns = [
    url(r'^detail/(?P<pk>\d+)/$', UserDetailView.as_view(), name='user_detail'),
    url(r'^login/$', CustomLoginView.as_view(), name='login'),
    url(r'^registration/$', RegisterCreateView.as_view(), name='registration'),
    url(r'^update/(?P<pk>\d+)/$', UserUpdateView.as_view(), name='update_user'),
    url(r'^logout/$', CustomLogoutView.as_view(), name='logout'),
]

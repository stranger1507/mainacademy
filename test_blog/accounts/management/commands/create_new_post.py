from pprint import pprint
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from bs4 import BeautifulSoup
import requests

from blog.models import Post


class Command(BaseCommand):
    help = 'I am your help text'

    def handle(self, *args, **options):
        data = {
            'amount': 100,
            'what': 'paras',
            'start': 'yes'
        }
        url = 'https://ru.lipsum.com/feed/html'
        response = requests.post(url, data)
        """
        text = models.TextField(_('Текст поста'))
        user = models.ForeignKey(
            settings.AUTH_USER_MODEL, verbose_name=_('Автор'), on_delete=models.CASCADE
        )
        title = models.CharField(_('Заголовок'), max_length=255)
        created_datetime = models.DateTimeField(_('Дата и время создания'), auto_now_add=True)
        edited_datetime = models.DateTimeField(_('Дата и время редактирования'), auto_now=True)
        ordering = models.PositiveIntegerField(_('Номер поста'), null=True, blank=True)
        is_published = models.BooleanField(_('Пост опубликован?'), default=False)
        """
        user = get_user_model().objects.first()
        if response.status_code == 200:
            soup = BeautifulSoup(response.text, 'html.parser')
            for p in soup.find('div', {'class': 'boxed'}).find_all('p'):
                print(p.text)
                print('-'*50)
                text = p.text.strip()
                Post.objects.create(user=user, text=text, title=text[:10], is_published=True)

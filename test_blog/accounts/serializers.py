from rest_framework import serializers
from .models import User


class UserSerializer(serializers.ModelSerializer):
    who_is = serializers.CharField(source='get_short_name', required=False)

    class Meta:
        model = User
        fields = (
            'id', 'first_name', 'last_name', 'get_full_name', 'is_active', 'last_login',
            'email', 'is_staff', 'date_joined', 'who_is',
        )
        read_only_fields = ('last_login', 'date_joined', 'is_staff', 'id', 'is_staff', 'who_is',)

import sys


if len(sys.argv) > 1 and 'test' in sys.argv:
    try:
        from .unittesting import *  # noqa
    except ImportError:
        pass

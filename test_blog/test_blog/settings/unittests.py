import os

from .local import *  # noqa


INSTALLED_APPS += ('django_nose',)  # noqa ignore=F999

TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'


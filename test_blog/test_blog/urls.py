import debug_toolbar
from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include
from rest_framework.routers import DefaultRouter
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='My API')
router = DefaultRouter()
urlpatterns = [
    url(r'admin/', admin.site.urls),
    url(r'accounts/', include('accounts.urls')),
    url(r'', include('blog.urls')),
    url(r'^captcha/', include('captcha.urls')),
    url(r'^docs/$', schema_view),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^__debug__/', include(debug_toolbar.urls))
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += router.urls

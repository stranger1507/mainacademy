import os

from celery import Celery

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'test_blog.settings.local')

app = Celery('mainacademy')  # noqa


# Using a string here means the worker will not have to
# pickle the object when using Windows.
app.config_from_object('django.conf:settings', namespace='CELERY')

# There is no need to pass args to autodiscover func in Celery >4.x
app.autodiscover_tasks()


@app.task
def debug_task():
    print('Request:')

@app.task
def new_super_duper_task():
    print('sdfdsfsdfsdfs')
    raise ValueError('hack dukask')
